<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "plantas_remedios".
 *
 * @property int $idcod
 * @property int $id_plantas
 * @property int $codigo_remedios
 *
 * @property Remedios $codigoRemedios
 * @property Plantas $plantas
 */
class PlantasRemedios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'plantas_remedios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_plantas', 'codigo_remedios'], 'required'],
            [['id_plantas', 'codigo_remedios'], 'integer'],
            [['codigo_remedios', 'id_plantas'], 'unique', 'targetAttribute' => ['codigo_remedios', 'id_plantas']],
            [['codigo_remedios'], 'exist', 'skipOnError' => true, 'targetClass' => Remedios::class, 'targetAttribute' => ['codigo_remedios' => 'codigo']],
            [['id_plantas'], 'exist', 'skipOnError' => true, 'targetClass' => Plantas::class, 'targetAttribute' => ['id_plantas' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idcod' => 'Idcod',
            'id_plantas' => 'Id Plantas',
            'codigo_remedios' => 'Codigo Remedios',
        ];
    }

    /**
     * Gets query for [[CodigoRemedios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoRemedios()
    {
        return $this->hasOne(Remedios::class, ['codigo' => 'codigo_remedios']);
    }

    /**
     * Gets query for [[Plantas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPlantas()
    {
        return $this->hasOne(Plantas::class, ['id' => 'id_plantas']);
    }
}
