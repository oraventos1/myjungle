<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "plantas_consejos".
 *
 * @property int $idcod
 * @property int|null $id_plantas
 * @property int|null $codigo_consejos
 *
 * @property Consejos $codigoConsejos
 * @property Plantas $plantas
 */
class PlantasConsejos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'plantas_consejos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_plantas', 'codigo_consejos'], 'integer'],
            [['codigo_consejos', 'id_plantas'], 'unique', 'targetAttribute' => ['codigo_consejos', 'id_plantas']],
            [['codigo_consejos'], 'exist', 'skipOnError' => true, 'targetClass' => Consejos::class, 'targetAttribute' => ['codigo_consejos' => 'codigo']],
            [['id_plantas'], 'exist', 'skipOnError' => true, 'targetClass' => Plantas::class, 'targetAttribute' => ['id_plantas' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idcod' => 'Idcod',
            'id_plantas' => 'Id Plantas',
            'codigo_consejos' => 'Codigo Consejos',
        ];
    }

    /**
     * Gets query for [[CodigoConsejos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoConsejos()
    {
        return $this->hasOne(Consejos::class, ['codigo' => 'codigo_consejos']);
    }

    /**
     * Gets query for [[Plantas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPlantas()
    {
        return $this->hasOne(Plantas::class, ['id' => 'id_plantas']);
    }
}
