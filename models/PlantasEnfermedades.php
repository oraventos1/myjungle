<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "plantas_enfermedades".
 *
 * @property int $codigo
 * @property int|null $id_plantas
 * @property string|null $nombre_enfermedades
 *
 * @property EnfermedadesPlagas $nombreEnfermedades
 * @property Plantas $plantas
 */
class PlantasEnfermedades extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'plantas_enfermedades';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_plantas'], 'integer'],
            [['nombre_enfermedades'], 'string', 'max' => 30],
            [['nombre_enfermedades', 'id_plantas'], 'unique', 'targetAttribute' => ['nombre_enfermedades', 'id_plantas']],
            [['nombre_enfermedades'], 'exist', 'skipOnError' => true, 'targetClass' => EnfermedadesPlagas::class, 'targetAttribute' => ['nombre_enfermedades' => 'nombre']],
            [['id_plantas'], 'exist', 'skipOnError' => true, 'targetClass' => Plantas::class, 'targetAttribute' => ['id_plantas' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'id_plantas' => 'Id Plantas',
            'nombre_enfermedades' => 'Nombre Enfermedades',
        ];
    }

    /**
     * Gets query for [[NombreEnfermedades]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombreEnfermedades()
    {
        return $this->hasOne(EnfermedadesPlagas::class, ['nombre' => 'nombre_enfermedades']);
    }

    /**
     * Gets query for [[Plantas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPlantas()
    {
        return $this->hasOne(Plantas::class, ['id' => 'id_plantas']);
    }
}
