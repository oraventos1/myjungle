<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "macetas".
 *
 * @property int $codigo
 * @property float|null $volumen
 * @property string $dimensiones
 * @property string $tipo_material
 *
 * @property Plantas[] $plantas
 */
class Macetas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'macetas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['volumen'], 'number'],
            [['dimensiones', 'tipo_material'], 'required'],
            [['dimensiones', 'tipo_material'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'volumen' => 'Volumen',
            'dimensiones' => 'Dimensiones',
            'tipo_material' => 'Tipo Material',
        ];
    }

    /**
     * Gets query for [[Plantas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPlantas()
    {
        return $this->hasMany(Plantas::class, ['codigo_maceta' => 'codigo']);
    }
}
