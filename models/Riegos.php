<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "riegos".
 *
 * @property int $codigo
 * @property float $cantidad
 * @property string $velocidad
 * @property string $fecha
 * @property int $id_planta
 * @property int $codigo_ambiental
 *
 * @property CondicionesAmbientales $codigoAmbiental
 * @property Plantas $planta
 */
class Riegos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'riegos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cantidad', 'velocidad', 'fecha', 'id_planta', 'codigo_ambiental'], 'required'],
            [['cantidad'], 'number'],
            [['velocidad'], 'string'],
            [['fecha'], 'safe'],
            [['id_planta', 'codigo_ambiental'], 'integer'],
            [['codigo_ambiental'], 'exist', 'skipOnError' => true, 'targetClass' => CondicionesAmbientales::class, 'targetAttribute' => ['codigo_ambiental' => 'codigo']],
            [['id_planta'], 'exist', 'skipOnError' => true, 'targetClass' => Plantas::class, 'targetAttribute' => ['id_planta' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'cantidad' => 'Cantidad',
            'velocidad' => 'Velocidad',
            'fecha' => 'Fecha',
            'id_planta' => 'Id Planta',
            'codigo_ambiental' => 'Codigo Ambiental',
        ];
    }

    /**
     * Gets query for [[CodigoAmbiental]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoAmbiental()
    {
        return $this->hasOne(CondicionesAmbientales::class, ['codigo' => 'codigo_ambiental']);
    }

    /**
     * Gets query for [[Planta]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPlanta()
    {
        return $this->hasOne(Plantas::class, ['id' => 'id_planta']);
    }
}
