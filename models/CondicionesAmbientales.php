<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "condiciones_ambientales".
 *
 * @property int $codigo
 * @property string $clima
 * @property int $viento
 * @property float $temperatura
 * @property string $luz
 * @property int $humedad
 *
 * @property Plantas[] $plantas
 * @property Riegos[] $riegos
 */
class CondicionesAmbientales extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'condiciones_ambientales';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['clima', 'viento', 'temperatura', 'luz', 'humedad'], 'required'],
            [['viento', 'humedad'], 'integer'],
            [['temperatura'], 'number'],
            [['clima'], 'string', 'max' => 20],
            [['luz'], 'string', 'max' => 40],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'clima' => 'Clima',
            'viento' => 'Viento',
            'temperatura' => 'Temperatura',
            'luz' => 'Luz',
            'humedad' => 'Humedad',
        ];
    }

    /**
     * Gets query for [[Plantas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPlantas()
    {
        return $this->hasMany(Plantas::class, ['codigo_ambiental' => 'codigo']);
    }

    /**
     * Gets query for [[Riegos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRiegos()
    {
        return $this->hasMany(Riegos::class, ['codigo_ambiental' => 'codigo']);
    }
}
