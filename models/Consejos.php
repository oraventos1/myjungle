<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "consejos".
 *
 * @property int $codigo
 * @property string $tipo
 * @property string $nivel_importancia
 * @property string $descripcion
 *
 * @property Plantas[] $plantas
 * @property PlantasConsejos[] $plantasConsejos
 */
class Consejos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'consejos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo', 'tipo', 'nivel_importancia', 'descripcion'], 'required'],
            [['codigo'], 'integer'],
            [['nivel_importancia'], 'string'],
            [['tipo'], 'string', 'max' => 30],
            [['descripcion'], 'string', 'max' => 1000],
            [['codigo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'tipo' => 'Tipo',
            'nivel_importancia' => 'Nivel Importancia',
            'descripcion' => 'Descripcion',
        ];
    }

    /**
     * Gets query for [[Plantas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPlantas()
    {
        return $this->hasMany(Plantas::class, ['id' => 'id_plantas'])->viaTable('plantas_consejos', ['codigo_consejos' => 'codigo']);
    }

    /**
     * Gets query for [[PlantasConsejos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPlantasConsejos()
    {
        return $this->hasMany(PlantasConsejos::class, ['codigo_consejos' => 'codigo']);
    }
}
