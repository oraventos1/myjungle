<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "enfermedades_plagas".
 *
 * @property string $nombre
 * @property string $descripcion
 *
 * @property Remedios[] $codigoRemedios
 * @property Plantas[] $plantas
 * @property PlantasEnfermedades[] $plantasEnfermedades
 * @property RemediosEnfermedades[] $remediosEnfermedades
 */
class EnfermedadesPlagas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'enfermedades_plagas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'descripcion'], 'required'],
            [['nombre'], 'string', 'max' => 30],
            [['descripcion'], 'string', 'max' => 10000],
            [['nombre'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
        ];
    }

    /**
     * Gets query for [[CodigoRemedios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoRemedios()
    {
        return $this->hasMany(Remedios::class, ['codigo' => 'codigo_remedios'])->viaTable('remedios_enfermedades', ['nombre_enfermedades' => 'nombre']);
    }

    /**
     * Gets query for [[Plantas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPlantas()
    {
        return $this->hasMany(Plantas::class, ['id' => 'id_plantas'])->viaTable('plantas_enfermedades', ['nombre_enfermedades' => 'nombre']);
    }

    /**
     * Gets query for [[PlantasEnfermedades]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPlantasEnfermedades()
    {
        return $this->hasMany(PlantasEnfermedades::class, ['nombre_enfermedades' => 'nombre']);
    }

    /**
     * Gets query for [[RemediosEnfermedades]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRemediosEnfermedades()
    {
        return $this->hasMany(RemediosEnfermedades::class, ['nombre_enfermedades' => 'nombre']);
    }
}
