<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "remedios".
 *
 * @property int $codigo
 * @property string $titulo
 * @property string $descripcion
 *
 * @property EnfermedadesPlagas[] $nombreEnfermedades
 * @property Plantas[] $plantas
 * @property PlantasRemedios[] $plantasRemedios
 * @property RemediosEnfermedades[] $remediosEnfermedades
 */
class Remedios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'remedios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['titulo', 'descripcion'], 'required'],
            [['titulo'], 'string', 'max' => 20],
            [['descripcion'], 'string', 'max' => 1000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'titulo' => 'Titulo',
            'descripcion' => 'Descripcion',
        ];
    }

    /**
     * Gets query for [[NombreEnfermedades]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombreEnfermedades()
    {
        return $this->hasMany(EnfermedadesPlagas::class, ['nombre' => 'nombre_enfermedades'])->viaTable('remedios_enfermedades', ['codigo_remedios' => 'codigo']);
    }

    /**
     * Gets query for [[Plantas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPlantas()
    {
        return $this->hasMany(Plantas::class, ['id' => 'id_plantas'])->viaTable('plantas_remedios', ['codigo_remedios' => 'codigo']);
    }

    /**
     * Gets query for [[PlantasRemedios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPlantasRemedios()
    {
        return $this->hasMany(PlantasRemedios::class, ['codigo_remedios' => 'codigo']);
    }

    /**
     * Gets query for [[RemediosEnfermedades]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRemediosEnfermedades()
    {
        return $this->hasMany(RemediosEnfermedades::class, ['codigo_remedios' => 'codigo']);
    }
}
