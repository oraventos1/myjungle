<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ubicaciones_interior".
 *
 * @property int $idcod
 * @property int $codigo_ubicaciones
 * @property string $interior
 *
 * @property Ubicaciones $codigoUbicaciones
 */
class UbicacionesInterior extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ubicaciones_interior';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idcod', 'codigo_ubicaciones', 'interior'], 'required'],
            [['idcod', 'codigo_ubicaciones'], 'integer'],
            [['interior'], 'string'],
            [['codigo_ubicaciones', 'interior'], 'unique', 'targetAttribute' => ['codigo_ubicaciones', 'interior']],
            [['codigo_ubicaciones'], 'exist', 'skipOnError' => true, 'targetClass' => Ubicaciones::class, 'targetAttribute' => ['codigo_ubicaciones' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idcod' => 'Idcod',
            'codigo_ubicaciones' => 'Codigo Ubicaciones',
            'interior' => 'Interior',
        ];
    }

    /**
     * Gets query for [[CodigoUbicaciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoUbicaciones()
    {
        return $this->hasOne(Ubicaciones::class, ['codigo' => 'codigo_ubicaciones']);
    }
}
