<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "remedios_enfermedades".
 *
 * @property int $id
 * @property string|null $nombre_enfermedades
 * @property int $codigo_remedios
 *
 * @property Remedios $codigoRemedios
 * @property EnfermedadesPlagas $nombreEnfermedades
 */
class RemediosEnfermedades extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'remedios_enfermedades';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_remedios'], 'required'],
            [['codigo_remedios'], 'integer'],
            [['nombre_enfermedades'], 'string', 'max' => 30],
            [['nombre_enfermedades', 'codigo_remedios'], 'unique', 'targetAttribute' => ['nombre_enfermedades', 'codigo_remedios']],
            [['codigo_remedios'], 'exist', 'skipOnError' => true, 'targetClass' => Remedios::class, 'targetAttribute' => ['codigo_remedios' => 'codigo']],
            [['nombre_enfermedades'], 'exist', 'skipOnError' => true, 'targetClass' => EnfermedadesPlagas::class, 'targetAttribute' => ['nombre_enfermedades' => 'nombre']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre_enfermedades' => 'Nombre Enfermedades',
            'codigo_remedios' => 'Codigo Remedios',
        ];
    }

    /**
     * Gets query for [[CodigoRemedios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoRemedios()
    {
        return $this->hasOne(Remedios::class, ['codigo' => 'codigo_remedios']);
    }

    /**
     * Gets query for [[NombreEnfermedades]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombreEnfermedades()
    {
        return $this->hasOne(EnfermedadesPlagas::class, ['nombre' => 'nombre_enfermedades']);
    }
}
