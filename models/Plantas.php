<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "plantas".
 *
 * @property int $id
 * @property string $nombre_cientifico
 * @property string $nombre_botanico
 * @property int $años
 * @property int $cm
 * @property string $descripcion
 * @property string $tipo_hoja
 * @property int $codigo_ambiental
 * @property int $codigo_maceta
 * @property int $codigo_ubicacion
 *
 * @property CondicionesAmbientales $codigoAmbiental
 * @property Consejos[] $codigoConsejos
 * @property Macetas $codigoMaceta
 * @property Remedios[] $codigoRemedios
 * @property Ubicaciones $codigoUbicacion
 * @property EnfermedadesPlagas[] $nombreEnfermedades
 * @property PlantasConsejos[] $plantasConsejos
 * @property PlantasEnfermedades[] $plantasEnfermedades
 * @property PlantasRemedios[] $plantasRemedios
 * @property Riegos[] $riegos
 */
class Plantas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'plantas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre_cientifico', 'nombre_botanico', 'años', 'cm', 'descripcion', 'tipo_hoja', 'codigo_ambiental', 'codigo_maceta', 'codigo_ubicacion'], 'required'],
            [['años', 'cm', 'codigo_ambiental', 'codigo_maceta', 'codigo_ubicacion'], 'integer'],
            [['nombre_cientifico'], 'string', 'max' => 40],
            [['nombre_botanico', 'tipo_hoja'], 'string', 'max' => 30],
            [['descripcion'], 'string', 'max' => 10000],
            [['codigo_ambiental'], 'exist', 'skipOnError' => true, 'targetClass' => CondicionesAmbientales::class, 'targetAttribute' => ['codigo_ambiental' => 'codigo']],
            [['codigo_maceta'], 'exist', 'skipOnError' => true, 'targetClass' => Macetas::class, 'targetAttribute' => ['codigo_maceta' => 'codigo']],
            [['codigo_ubicacion'], 'exist', 'skipOnError' => true, 'targetClass' => Ubicaciones::class, 'targetAttribute' => ['codigo_ubicacion' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre_cientifico' => 'Nombre Cientifico',
            'nombre_botanico' => 'Nombre Botanico',
            'años' => 'Años',
            'cm' => 'Cm',
            'descripcion' => 'Descripcion',
            'tipo_hoja' => 'Tipo Hoja',
            'codigo_ambiental' => 'Codigo Ambiental',
            'codigo_maceta' => 'Codigo Maceta',
            'codigo_ubicacion' => 'Codigo Ubicacion',
        ];
    }

    /**
     * Gets query for [[CodigoAmbiental]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoAmbiental()
    {
        return $this->hasOne(CondicionesAmbientales::class, ['codigo' => 'codigo_ambiental']);
    }

    /**
     * Gets query for [[CodigoConsejos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoConsejos()
    {
        return $this->hasMany(Consejos::class, ['codigo' => 'codigo_consejos'])->viaTable('plantas_consejos', ['id_plantas' => 'id']);
    }

    /**
     * Gets query for [[CodigoMaceta]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoMaceta()
    {
        return $this->hasOne(Macetas::class, ['codigo' => 'codigo_maceta']);
    }

    /**
     * Gets query for [[CodigoRemedios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoRemedios()
    {
        return $this->hasMany(Remedios::class, ['codigo' => 'codigo_remedios'])->viaTable('plantas_remedios', ['id_plantas' => 'id']);
    }

    /**
     * Gets query for [[CodigoUbicacion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoUbicacion()
    {
        return $this->hasOne(Ubicaciones::class, ['codigo' => 'codigo_ubicacion']);
    }

    /**
     * Gets query for [[NombreEnfermedades]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombreEnfermedades()
    {
        return $this->hasMany(EnfermedadesPlagas::class, ['nombre' => 'nombre_enfermedades'])->viaTable('plantas_enfermedades', ['id_plantas' => 'id']);
    }

    /**
     * Gets query for [[PlantasConsejos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPlantasConsejos()
    {
        return $this->hasMany(PlantasConsejos::class, ['id_plantas' => 'id']);
    }

    /**
     * Gets query for [[PlantasEnfermedades]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPlantasEnfermedades()
    {
        return $this->hasMany(PlantasEnfermedades::class, ['id_plantas' => 'id']);
    }

    /**
     * Gets query for [[PlantasRemedios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPlantasRemedios()
    {
        return $this->hasMany(PlantasRemedios::class, ['id_plantas' => 'id']);
    }

    /**
     * Gets query for [[Riegos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRiegos()
    {
        return $this->hasMany(Riegos::class, ['id_planta' => 'id']);
    }
}
