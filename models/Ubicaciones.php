<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ubicaciones".
 *
 * @property int $codigo
 * @property string $dirección
 *
 * @property Plantas[] $plantas
 * @property UbicacionesInterior[] $ubicacionesInteriors
 */
class Ubicaciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ubicaciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dirección'], 'required'],
            [['dirección'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'dirección' => 'Dirección',
        ];
    }

    /**
     * Gets query for [[Plantas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPlantas()
    {
        return $this->hasMany(Plantas::class, ['codigo_ubicacion' => 'codigo']);
    }

    /**
     * Gets query for [[UbicacionesInteriors]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUbicacionesInteriors()
    {
        return $this->hasMany(UbicacionesInterior::class, ['codigo_ubicaciones' => 'codigo']);
    }
}
