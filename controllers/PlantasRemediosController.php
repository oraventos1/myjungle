<?php

namespace app\Controllers;

use app\models\plantasremedios;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PlantasRemediosController implements the CRUD actions for plantasremedios model.
 */
class PlantasRemediosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all plantasremedios models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => plantasremedios::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'idcod' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single plantasremedios model.
     * @param int $idcod Idcod
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idcod)
    {
        return $this->render('view', [
            'model' => $this->findModel($idcod),
        ]);
    }

    /**
     * Creates a new plantasremedios model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new plantasremedios();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'idcod' => $model->idcod]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing plantasremedios model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $idcod Idcod
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idcod)
    {
        $model = $this->findModel($idcod);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idcod' => $model->idcod]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing plantasremedios model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $idcod Idcod
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idcod)
    {
        $this->findModel($idcod)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the plantasremedios model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $idcod Idcod
     * @return plantasremedios the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idcod)
    {
        if (($model = plantasremedios::findOne(['idcod' => $idcod])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
