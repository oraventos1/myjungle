<?php

namespace app\controllers;

use app\models\ubicacionesinterior;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UbicacionesInteriorController implements the CRUD actions for ubicacionesinterior model.
 */
class UbicacionesInteriorController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all ubicacionesinterior models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => ubicacionesinterior::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigo_ubicaciones' => SORT_DESC,
                    'interior' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ubicacionesinterior model.
     * @param int $codigo_ubicaciones Codigo Ubicaciones
     * @param string $interior Interior
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigo_ubicaciones, $interior)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigo_ubicaciones, $interior),
        ]);
    }

    /**
     * Creates a new ubicacionesinterior model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new ubicacionesinterior();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigo_ubicaciones' => $model->codigo_ubicaciones, 'interior' => $model->interior]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ubicacionesinterior model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codigo_ubicaciones Codigo Ubicaciones
     * @param string $interior Interior
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigo_ubicaciones, $interior)
    {
        $model = $this->findModel($codigo_ubicaciones, $interior);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_ubicaciones' => $model->codigo_ubicaciones, 'interior' => $model->interior]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ubicacionesinterior model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codigo_ubicaciones Codigo Ubicaciones
     * @param string $interior Interior
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigo_ubicaciones, $interior)
    {
        $this->findModel($codigo_ubicaciones, $interior)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ubicacionesinterior model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codigo_ubicaciones Codigo Ubicaciones
     * @param string $interior Interior
     * @return ubicacionesinterior the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigo_ubicaciones, $interior)
    {
        if (($model = ubicacionesinterior::findOne(['codigo_ubicaciones' => $codigo_ubicaciones, 'interior' => $interior])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
