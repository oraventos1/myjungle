<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\plantasconsejos $model */

$this->title = 'Update Plantasconsejos: ' . $model->idcod;
$this->params['breadcrumbs'][] = ['label' => 'Plantasconsejos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idcod, 'url' => ['view', 'idcod' => $model->idcod]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="plantasconsejos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
