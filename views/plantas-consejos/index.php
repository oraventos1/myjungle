<?php

use app\models\plantasconsejos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Plantasconsejos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="plantasconsejos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Plantasconsejos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idcod',
            'id_plantas',
            'codigo_consejos',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, plantasconsejos $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'idcod' => $model->idcod]);
                 }
            ],
        ],
    ]); ?>


</div>
