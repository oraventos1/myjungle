<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\plantasconsejos $model */

$this->title = 'Create Plantasconsejos';
$this->params['breadcrumbs'][] = ['label' => 'Plantasconsejos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="plantasconsejos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
