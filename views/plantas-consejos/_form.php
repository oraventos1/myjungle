<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\plantasconsejos $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="plantasconsejos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_plantas')->textInput() ?>

    <?= $form->field($model, 'codigo_consejos')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
