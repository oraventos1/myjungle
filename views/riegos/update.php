<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\riegos $model */

$this->title = 'Update Riegos: ' . $model->codigo;
$this->params['breadcrumbs'][] = ['label' => 'Riegos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo, 'url' => ['view', 'codigo' => $model->codigo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="riegos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
