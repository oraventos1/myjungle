<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\riegos $model */

$this->title = 'Create Riegos';
$this->params['breadcrumbs'][] = ['label' => 'Riegos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="riegos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
