<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\riegos $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="riegos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cantidad')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'velocidad')->dropDownList([ 'lento' => 'Lento', 'normal' => 'Normal', 'rapido' => 'Rapido', '' => '', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'fecha')->textInput() ?>

    <?= $form->field($model, 'id_planta')->textInput() ?>

    <?= $form->field($model, 'codigo_ambiental')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
