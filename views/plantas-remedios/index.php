<?php

use app\models\plantasremedios;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Plantasremedios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="plantasremedios-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Plantasremedios', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idcod',
            'id_plantas',
            'codigo_remedios',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, plantasremedios $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'idcod' => $model->idcod]);
                 }
            ],
        ],
    ]); ?>


</div>
