<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\plantasremedios $model */

$this->title = $model->idcod;
$this->params['breadcrumbs'][] = ['label' => 'Plantasremedios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="plantasremedios-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'idcod' => $model->idcod], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'idcod' => $model->idcod], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idcod',
            'id_plantas',
            'codigo_remedios',
        ],
    ]) ?>

</div>
