<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\plantasremedios $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="plantasremedios-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_plantas')->textInput() ?>

    <?= $form->field($model, 'codigo_remedios')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
