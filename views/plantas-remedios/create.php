<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\plantasremedios $model */

$this->title = 'Create Plantasremedios';
$this->params['breadcrumbs'][] = ['label' => 'Plantasremedios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="plantasremedios-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
