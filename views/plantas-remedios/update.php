<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\plantasremedios $model */

$this->title = 'Update Plantasremedios: ' . $model->idcod;
$this->params['breadcrumbs'][] = ['label' => 'Plantasremedios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idcod, 'url' => ['view', 'idcod' => $model->idcod]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="plantasremedios-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
