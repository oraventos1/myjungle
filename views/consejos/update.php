<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\consejos $model */

$this->title = 'Update Consejos: ' . $model->codigo;
$this->params['breadcrumbs'][] = ['label' => 'Consejos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo, 'url' => ['view', 'codigo' => $model->codigo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="consejos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
