<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ubicaciones $model */

$this->title = 'Create Ubicaciones';
$this->params['breadcrumbs'][] = ['label' => 'Ubicaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ubicaciones-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
