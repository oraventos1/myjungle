<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\remediosenfermedades $model */

$this->title = 'Create Remediosenfermedades';
$this->params['breadcrumbs'][] = ['label' => 'Remediosenfermedades', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="remediosenfermedades-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
