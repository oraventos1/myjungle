<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\ubicacionesinterior $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="ubicacionesinterior-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idcod')->textInput() ?>

    <?= $form->field($model, 'codigo_ubicaciones')->textInput() ?>

    <?= $form->field($model, 'interior')->dropDownList([ 'interior' => 'Interior', 'exterior' => 'Exterior', 'ambos' => 'Ambos', '' => '', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
