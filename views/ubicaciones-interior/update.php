<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ubicacionesinterior $model */

$this->title = 'Update Ubicacionesinterior: ' . $model->codigo_ubicaciones;
$this->params['breadcrumbs'][] = ['label' => 'Ubicacionesinteriors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_ubicaciones, 'url' => ['view', 'codigo_ubicaciones' => $model->codigo_ubicaciones, 'interior' => $model->interior]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ubicacionesinterior-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
