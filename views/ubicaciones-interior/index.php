<?php

use app\models\ubicacionesinterior;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Ubicacionesinteriors';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ubicacionesinterior-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Ubicacionesinterior', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idcod',
            'codigo_ubicaciones',
            'interior',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, ubicacionesinterior $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'codigo_ubicaciones' => $model->codigo_ubicaciones, 'interior' => $model->interior]);
                 }
            ],
        ],
    ]); ?>


</div>
