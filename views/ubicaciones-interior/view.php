<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\ubicacionesinterior $model */

$this->title = $model->codigo_ubicaciones;
$this->params['breadcrumbs'][] = ['label' => 'Ubicacionesinteriors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ubicacionesinterior-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'codigo_ubicaciones' => $model->codigo_ubicaciones, 'interior' => $model->interior], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'codigo_ubicaciones' => $model->codigo_ubicaciones, 'interior' => $model->interior], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idcod',
            'codigo_ubicaciones',
            'interior',
        ],
    ]) ?>

</div>
