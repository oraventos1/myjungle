<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\plantas $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="plantas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre_cientifico')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombre_botanico')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'años')->textInput() ?>

    <?= $form->field($model, 'cm')->textInput() ?>

    <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tipo_hoja')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'codigo_ambiental')->textInput() ?>

    <?= $form->field($model, 'codigo_maceta')->textInput() ?>

    <?= $form->field($model, 'codigo_ubicacion')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
