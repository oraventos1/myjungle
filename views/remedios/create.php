<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\remedios $model */

$this->title = 'Create Remedios';
$this->params['breadcrumbs'][] = ['label' => 'Remedios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="remedios-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
