<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\macetas $model */

$this->title = 'Create Macetas';
$this->params['breadcrumbs'][] = ['label' => 'Macetas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="macetas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
