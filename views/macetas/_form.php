<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\macetas $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="macetas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'volumen')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dimensiones')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tipo_material')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
