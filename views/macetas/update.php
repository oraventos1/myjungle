<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\macetas $model */

$this->title = 'Update Macetas: ' . $model->codigo;
$this->params['breadcrumbs'][] = ['label' => 'Macetas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo, 'url' => ['view', 'codigo' => $model->codigo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="macetas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
