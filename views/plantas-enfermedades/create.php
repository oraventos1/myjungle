<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\plantasenfermedades $model */

$this->title = 'Create Plantasenfermedades';
$this->params['breadcrumbs'][] = ['label' => 'Plantasenfermedades', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="plantasenfermedades-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
