<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\plantasenfermedades $model */

$this->title = 'Update Plantasenfermedades: ' . $model->codigo;
$this->params['breadcrumbs'][] = ['label' => 'Plantasenfermedades', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo, 'url' => ['view', 'codigo' => $model->codigo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="plantasenfermedades-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
