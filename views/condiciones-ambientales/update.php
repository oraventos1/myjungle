<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\condicionesambientales $model */

$this->title = 'Update Condicionesambientales: ' . $model->codigo;
$this->params['breadcrumbs'][] = ['label' => 'Condicionesambientales', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo, 'url' => ['view', 'codigo' => $model->codigo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="condicionesambientales-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
