<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\condicionesambientales $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="condicionesambientales-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'clima')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'viento')->textInput() ?>

    <?= $form->field($model, 'temperatura')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'luz')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'humedad')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
